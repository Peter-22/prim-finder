package dev.kovacsp.primfinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimFinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrimFinderApplication.class, args);
	}

}
