package dev.kovacsp.primfinder.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dev.kovacsp.primfinder.service.PrimeFinderService;

@RestController
@RequestMapping("prim-finder/")
public class PrimFinderController {

	private final PrimeFinderService finderService;

	public PrimFinderController(PrimeFinderService finderService) {
		this.finderService = finderService;
	}

	@GetMapping("/start")
	public ResponseEntity<String> startPrimFinder(
			@RequestParam(required = false, defaultValue = "1") int numberOfThreads) {
		return finderService.startPrimFinder(numberOfThreads);
	}

	@GetMapping("/result")
	public List<Integer> getPrimNumbersByLimits(@RequestParam Integer lowerLomit, @RequestParam Integer upperLimit) {
		return finderService.getPrimNumbersByLimits(lowerLomit, upperLimit);
	}

	@GetMapping("/stop")
	public ResponseEntity<String> stopPrimFinder() {
		return finderService.stopPrimFinder();
	}

}
