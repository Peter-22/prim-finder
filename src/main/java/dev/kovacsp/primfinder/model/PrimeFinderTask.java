package dev.kovacsp.primfinder.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class PrimeFinderTask implements Callable<List<Integer>> {

	private int lowerLimit;
	private int upperLimit;
	private List<Integer> list = new ArrayList<Integer>();

	public PrimeFinderTask(int lowerLimit, int upperLimit) {
		this.lowerLimit = lowerLimit;
		this.upperLimit = upperLimit;
	}

	@Override
	public List<Integer> call() throws Exception {
		for (int i = lowerLimit; i < upperLimit; i++) {
			if (isPrime(i)) {
				list.add(i);
			}
		}
		return list;
	}

	private boolean isPrime(int num) {
		for (int i = 2; i < num; i++) {
			if (num % i == 0) {
				return false;
			}
		}
		return true;
	}

}
