package dev.kovacsp.primfinder.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import dev.kovacsp.primfinder.model.PrimeFinderTask;

@Service
public class PrimeFinderService {

	private ExecutorService executorService;

	private List<Integer> foundedPrimNumberList = new ArrayList<Integer>();
	private List<Future<List<Integer>>> futureList = new ArrayList<Future<List<Integer>>>();

	private static final int MAX_NUMBER_OF_THREADS = 5;
	private static final int UPPER_LIMIT_OF_SEARCH = 500_000;
	private static final int NUMBER_OF_SUBTASKS = 10;
	private static final int MAX_NUMBER_OF_RESULT_LIST = 30;

	
	public ResponseEntity<String> startPrimFinder(int requiredNumberOfThreads) {
		if (!isExecutorRunning()) {

			executorService = Executors.newFixedThreadPool(getNumberOfThreads(requiredNumberOfThreads));
	
			List<PrimeFinderTask> subTaskList = createSubtasks();
			executeSubtasks(subTaskList);
			
			executorService.shutdown();

			return ResponseEntity.status(HttpStatus.OK).body("The prime number search is started.");
		} else {
			return ResponseEntity.status(HttpStatus.CONFLICT)
					.body("The search cannot be started because the previous search is still in progress!");
		}
	}

	public List<Integer> getPrimNumbersByLimits(Integer lowerLomit, Integer upperLimit) {
		if (lowerLomit >= upperLimit)
			throw new IllegalArgumentException("The upper limit must be greater than the lower limit.");
		
		foundedPrimNumberList.clear();
		getAlreadyFoundPrimNumbers();
		return filterResultByLimits(lowerLomit, upperLimit);
	}

	public ResponseEntity<String> stopPrimFinder() {
		if (isExecutorRunning()) {
			executorService.shutdownNow();
			return ResponseEntity.status(HttpStatus.OK).body("The the service is being stopped.");
		} else {
			return ResponseEntity.status(HttpStatus.OK).body("The service is not running at the moment.");
		}
	}

	private int getNumberOfThreads(Integer numberOfThreads) {
		if (numberOfThreads < 1 || numberOfThreads > MAX_NUMBER_OF_THREADS) {
			return 1;
		}
		return numberOfThreads;
	}

	private List<PrimeFinderTask> createSubtasks() {
		List<PrimeFinderTask> subTaskList = new ArrayList<PrimeFinderTask>();

		for (int i = 0; i < NUMBER_OF_SUBTASKS; i++) {
			subTaskList.add(new PrimeFinderTask(Math.max(2, i * UPPER_LIMIT_OF_SEARCH / NUMBER_OF_SUBTASKS),
					(i + 1) * UPPER_LIMIT_OF_SEARCH / NUMBER_OF_SUBTASKS));
		}
		return subTaskList;
	}

	private void executeSubtasks(List<PrimeFinderTask> subTaskList) {
		futureList.clear();
		for (PrimeFinderTask primeFinderTask : subTaskList) {
			futureList.add(executorService.submit(primeFinderTask));
		}
	}

	private void getAlreadyFoundPrimNumbers() {
		if (futureList != null && !futureList.isEmpty()) {
			for (Future<List<Integer>> future : futureList) {
				if (future.isDone()) {
					try {
						foundedPrimNumberList.addAll(future.get());
					} catch (InterruptedException | ExecutionException e) {
						e.printStackTrace();
						throw new IllegalStateException("An error occurred while querying the prime numbers.");
					}
				}
			}
		}
	}

	private List<Integer> filterResultByLimits(Integer lowerLomit, Integer upperLimit) {
		List<Integer> resultList = foundedPrimNumberList.stream().filter(t -> t > lowerLomit && t < upperLimit)
				.toList();

		if (resultList.size() > MAX_NUMBER_OF_RESULT_LIST) {
			throw new IllegalArgumentException("You can request a maximum of " + MAX_NUMBER_OF_RESULT_LIST
					+ " prime numbers at once. Please modify the limits.");
		}

		return resultList;
	}

	private boolean isExecutorRunning() {
		return executorService != null && !executorService.isTerminated();
	}

}
